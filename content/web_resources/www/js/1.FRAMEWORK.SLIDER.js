(function (factory) {

    var HTMLElement = typeof HTMLElement !== "undefined" ? HTMLElement : Element;

    HTMLElement.prototype.Slider = factory;
}(function factory (config) {

    'use strict';

    var element         = this,
        //element    = null,
        // CONFIG PARAMS
        speed           = null,
        duration        = null,
        auto            = null,
        touch           = null,
        position        = null,
        controls        = null,
        prev            = null,
        next            = null,
        navigation      = null,
        size            = null,
        // CONTENT DATA
        content         = null,
        contentWidth    = null,
        contentHeight   = null,
        contents        = element.getElementsByClassName('sub') || element.children,
        contentsLength  = contents.length,
        contentsFirst   = null,
        contentsLast    = null,
        subContent      = null,
        contentIndex    = 0,
        currentContent  = 0,
        // TOUCH DATA
        touchStartTime  = null,
        touchDistance   = null,
        touchMoving     = null,
        touchRatio      = null,
        // TOUCH POSITIONS X
        touchStartX     = null,
        touchEndX       = null,
        touchMoveX      = null,
        movingX         = null,
        // TOUCH POSITIONS Y
        touchStartY     = null,
        touchEndY       = null,
        touchMoveY      = null,
        // ANOTHER VARS
        elapsedTime     = null,
        allowedTime     = 300,
        isFirstContent  = false,
        isLastContent   = false,
        width           = 100,
        height          = 100,
        timeout         = null,
        arrPrefix       = ['webkit', 'moz', 'ms', 'o'];

    element.init                = function () {

        if (config && typeof config === 'object') {

            auto        = config.auto;
            touch       = config.touch      ? config.touch      : false;
            position    = config.position   ? config.position   : 'horizontal';
            duration    = config.duration   ? config.duration   : 4000;
            speed       = config.speed      ? config.speed      : 300;
            controls    = config.controls   ? config.controls   : false;
            size        = config.size       ? config.size       : 'full';

            if (controls) {

                if (controls.prev && controls.next) {

                    prev = document.getElementById(controls.prev) || document.getElementsByClassName(controls.prev)[0];
                    next = document.getElementById(controls.next) || document.getElementsByClassName(controls.next)[0];
                }

                if (controls.navigation) {

                    navigation = document.getElementById(controls.navigation) || document.getElementsByClassName(controls.navigation)[0];
                }
            }

            if (!element.getAttribute('data-init')) {

                if (element.instance()) {

                    element.controls();
                    element.events();

                    if (auto) {

                        element.start();
                    }
                }
            }
        }
    };

    element.instance           = function () {

        var clones  = null;

        element.elementClones      = element.cloneNode(true);

        element.style.overflow  = 'hidden';
        element.style.height    = 'auto';

        element.setAttribute('data-init', true);

        /*
        ** NOVO CONTENT PARA OS SLIDERS
        *******/
        subContent  = document.createElement('div');

        /*
        ** CONTENTS DATA
        *******/
        contents        = element.children;
        contentsLength  = contents.length;

        if (position === 'vertical') {

            element.style.height    = contents[0].clientHeight + 'px';
        }

        for (var index = 0; index < contentsLength; index++) {

            if (typeof contents[index] === 'object') {

                /*
                ** CLONANDO CONTEUDO ORIGINAL
                *******/
                clones = contents[index].cloneNode(true);

                /*
                ** CSS DOS CLONES
                *******/
                if (position === 'vertical') {

                    clones.style.height  = ((size === 'semi' ? 90 : 100) / contentsLength) + '%';
                } else {

                    clones.style.width  = ((size === 'semi' ? 90 : 100) / contentsLength) + '%';
                }

                clones.style.float  = 'left';

                /*
                ** ADD ID CLONE
                *******/
                //clones.id = 'subContent_' + index;
                clones.classList.add('sub');

                if (parseInt(index, 10) === 0) {

                    clones.setAttribute('data-actived', true);
                }

                /*
                ** ADD INDEX CLONE
                *******/
                clones.setAttribute('data-index', index);

                /*
                ** INSERINO NOVO CONTEUDO
                *******/
                subContent.appendChild(clones);
            }
        }

        /*
        ** ADD ID
        *******/
        subContent.id = 'subContent';


        /*
        ** CSS SUBCONTENT
        *******/
        subContent.style.display = 'inline-block';

        if (position === 'vertical') {

            subContent.style.height = (100 * contentsLength) + '%';
        } else {

            subContent.style.width  = (100 * contentsLength) + '%';
        }

        /*
        ** NOVO CONTEUDO DO SLIDER
        *******/
        element.innerHTML = subContent.outerHTML;

        return true;
    };

    element.events             = function (remove) {

        var events  = null,
            prefix  = '';

        if (window.addEventListener) {

            events = remove ? 'removeEventListener' : 'addEventListener';
        } else if (window.attachEvent) {

            events = remove ? 'detachEvent' : 'attachEvent';
            prefix = 'on';
        }

        /*
        ** TOUCH EVENTS
        *******/
        if (touch) {

            element[events](prefix + 'touchstart', element.touchStart, false);
            element[events](prefix + 'touchmove', element.touchMove, false);
            element[events](prefix + 'touchend', element.touchEnd, false);
        }

        /*
        ** CONTROL EVENTS
        *******/
        if (controls) {

            if (prev && next) {

                if (contentsLength > 1) {

                    prev[events](prefix + 'click', element.prev, false);
                    next[events](prefix + 'click', element.next, false);
                } else {

                    prev.style.display = 'none';
                    next.style.display = 'none';
                }
            }
        }

        /*
        ** NAVIGATION EVENTS
        *******/
        if (navigation) {

            for (var index = 0; index < contentsLength; index++) {

                if (navigation.children['nav_' + index]) {

                    navigation.children['nav_' + index][events](prefix + 'click', element.navigation, false);
                }
            }
        }

        /*
        ** WINDOW EVENTS
        *******/
        window[events](prefix + 'focus', element.start, false);
        window[events](prefix + 'blur', element.stop, false);
    };

    element.start              = function () {

        if (auto) {

            element.stop();

            timeout = setTimeout(element.next, (duration + speed));
        }
    };

    element.stop               = function () {

        clearTimeout(timeout);
    };

    element.data               = function () {

        width = size === 'semi' ? 90 : 100;

        /*
        ** CONTENT DATA
        *******/
        subContent      = element.childNodes[0];
        contentWidth    = size === 'semi' ? ((element.offsetWidth * 90) / 100) : element.offsetWidth;
        contentHeight   = element.offsetHeight;

        contentsFirst   = subContent.firstChild;
        contentsLast    = subContent.lastChild;

        contentIndex = parseInt(subContent.querySelectorAll('[data-actived="true"]')[0].getAttribute('data-index'), 10);

        if (contentIndex === 0) {

            isFirstContent = true;
        } else {

            isFirstContent = false;
        }

        if (contentIndex === (contentsLength - 1)) {

            isLastContent = true;
        } else {

            isLastContent = false;
        }
    };

    element.touchStart         = function (event) {

        element.data();

        element.stop();

        /*
        ** INCIO DO TOQUE
        *******/
        touchStartTime  = new Date().getTime();

        /*
        ** DISTANCIA DO TOQUE
        *******/
        touchDistance   = (contentWidth * -1);

        /*
        ** POSIÃ‡ÃƒO INICIAL DO TOQUE X
        *******/
        touchStartX     = (event.changedTouches[0].pageX);
        touchStartY     = (event.changedTouches[0].pageY);
    };

    element.touchMove          = function (event) {

        element.data();
        /*
        ** POSIÃ‡ÃƒO EM MOVIMENTO DO TOQUE X
        *******/
        touchMoveX  = (event.changedTouches[0].pageX);
        touchMoveY  = (event.changedTouches[0].pageY);

        touchRatio = 1;

        if ((element.touchDirection() === 'RIGHT' && isFirstContent) || (element.touchDirection() === 'LEFT' && isLastContent)) {

            touchRatio = 3;
        }

        if (element.touchDirection() === 'LEFT' || element.touchDirection() === 'RIGHT') {

            /*
            ** SE O TOUCH FOR MOVIMENTADO
            *******/
            touchMoving     = true;

            movingX         = (touchMoveX - touchStartX);

            touchDistance   = (movingX / touchRatio) - (contentIndex * contentWidth);

            subContent.style.transform = 'translateX(' + touchDistance + 'px)';
        }
    };

    element.touchEnd           = function (event) {

        element.data();

        /*
        ** REMOVENDO EVENTS
        *******/
        element.events(true);

        /*
        ** POSIÃ‡ÃƒO DO TOQUE FINAL
        *******/
        touchEndX  = (event.changedTouches[0].pageX);
        touchEndY  = (event.changedTouches[0].pageY);

        if (touchMoving) {

            elapsedTime = (new Date().getTime() - touchStartTime);

            element.addTransition();

            if (element.touchDirection() === 'LEFT') {

                element.touchSlideToLeft();
            } else if (element.touchDirection() === 'RIGHT') {

                element.touchSlideToRight();
            }
        } else {

            element.events();
        }

        if (element.touchDirection() === 'UP' || element.touchDirection() === 'DOWN') {

            element.events();
        }

        touchMoving     = false;
        isFirstContent  = false;
        isLastContent   = false;

        if (window.addEventListener) {

            element.addEffect();
        }
    };

    element.touchAngle         = function () {

        var x       = (touchStartX - touchMoveX),
            y       = (touchStartY - touchMoveY),
            radius  = Math.atan2(y, x),
            angle   = Math.round(radius * 180 / Math.PI);

        return (angle < 0) ? (360 - Math.abs(angle)) : angle;
    };

    element.touchDirection     = function () {

        var angle = element.touchAngle();

        if ((angle >= 0 && angle <= 45) || (angle >= 315 && angle <= 360)) {

            return 'LEFT';
        } else if (angle >= 45 && angle <= 135) {

            return 'UP';
        } else if (angle >= 135 && angle <= 225) {

            return 'RIGHT';
        } else {

            return 'DOWN';
        }
    };

    element.touchSlideToLeft   = function () {

        if ((elapsedTime <= allowedTime) || (movingX * -1) >= (contentWidth / 2)) {

            if (!isLastContent) {

                subContent.style.transform  = 'translateX(' + (((contentIndex + 1) * (width / contentsLength)) * -1) + '%)';

                currentContent = (contentIndex + 1);
            } else {

                subContent.style.transform = 'translateX(' + ((contentIndex * (width / contentsLength)) * -1) + '%)';
            }
        } else {

            subContent.style.transform = 'translateX(' + ((contentIndex * (width / contentsLength)) * -1) + '%)';
        }
    };

    element.touchSlideToRight  = function () {

        if ((elapsedTime <= allowedTime) || movingX >= (contentWidth / 2)) {

            if (!isFirstContent) {

                subContent.style.transform = 'translateX(' + (((contentIndex - 1) * (width / contentsLength)) * -1) + '%)';

                currentContent = (contentIndex - 1);
            } else {

                subContent.style.transform = 'translateX(0)';
            }
        } else {

            subContent.style.transform = 'translateX(' + ((contentIndex * (width / contentsLength)) * -1) + '%)';
        }
    };

    element.addTransition      = function () {

        for (var index = 0; index < arrPrefix.length; index++) {

            subContent.style[arrPrefix[index] + 'TransitionProperty']       = 'transform';
            subContent.style[arrPrefix[index] + 'TransitionTimingFunction'] = 'ease';
            subContent.style[arrPrefix[index] + 'TransitionDuration']       = (speed / 1000) + 's';
        }
    };

    element.addEffect          = function () {

        if (navigator.appVersion.indexOf("MSIE 7.") === -1 && navigator.appVersion.indexOf("MSIE 8.") === -1) {

            setTimeout(element.removeEffect, speed);
        } else {

            subContent.addEventListener('transitionend', element.removeEffect, false);
        }
    };

    element.removeEffect       = function () {

        /*
        ** REMOVENDO TRANSITION EFFECT
        *******/
        subContent.style.transition = 'none';

        /*
        ** REMOVENDO EVENT DO TRANSITION
        *******/
        subContent.removeEventListener('transitionend', element.removeEffect, false);

        for (var index = 0; index < contentsLength; index++) {

            if (subContent.children[index].nodeType === 1) {

                subContent.children[index].removeAttribute('data-actived');

                if (navigation) {

                    navigation.children[index].classList.remove('actived');
                }
            }
        }

        subContent.children[currentContent].setAttribute('data-actived', true);

        if (navigation) {

            navigation.children[currentContent].classList.add('actived');
        }

        element.start();

        /*
        ** DEVOLVENDO EVENTS
        *******/
        element.events();
    };

    element.addStyle           = function (el, property, value) {

        for (var index = 0; index < arrPrefix.length; index++) {

            el.style[arrPrefix[index] + property.charAt(0).toUpperCase() + property.slice(1)] = value;
        }
    };

    element.controls           = function () {

        if (navigator.appVersion.indexOf("MSIE 7.") === -1 && navigator.appVersion.indexOf("MSIE 8.") === -1) {

            /*
            ** DATA
            *******/
            element.data();

            var button = null;

            if (navigation) {

                for (var index = 0; index < contentsLength; index++) {

                    if (subContent.children[index].nodeType === 1) {

                        button = document.createElement('button');

                        button.id = 'nav_' + parseInt(index, 10);

                        if (index === 0) {

                            button.classList.add('actived');
                        }

                        button.value        = parseInt(index, 10);
                        button.innerHTML    = '<span><b>' + parseInt(index, 10) + '</b></span>';

                        navigation.appendChild(button);
                    }
                }
            }
        }
    };

    element.next               = function (event) {

        /*
        ** DATA
        *******/
        element.data();
        element.addTransition();
        element.stop();

        if (!isLastContent) {

            currentContent = parseInt((currentContent + 1) < contentsLength ? (currentContent + 1) : (contentsLength - 1), 10);

            if (subContent.style.transform !== undefined) {

                if (position === 'vertical') {

                    element.addStyle(subContent, 'transform', 'translateY(' + ((currentContent * (height / contentsLength)) * -1) + '%)');
                } else {

                    element.addStyle(subContent, 'transform', 'translateX(' + ((currentContent * (width / contentsLength)) * -1) + '%)');
                }
            } else {

                if (position === 'vertical') {

                    subContent.style.marginTop = ((currentContent * height) * -1) + '%';
                } else {

                    subContent.style.marginLeft = ((currentContent * width) * -1) + '%';
                }
            }

            if (window.addEventListener) {

                element.addEffect();
            }
        } else {

            currentContent = 0;

            element.navigation(currentContent);
        }


        element.start();

        return false;
    };

    element.prev               = function (event) {

        /*
        ** DATA
        *******/
        element.data();

        element.addTransition();

        element.stop();

        if (!isFirstContent) {

            currentContent = parseInt((currentContent - 1) < 0 ? 0 : (currentContent - 1), 10);

            if (subContent.style.transform !== undefined) {

                if (position === 'vertical') {

                    element.addStyle(subContent, 'transform', 'translateY(' + ((currentContent * (height / contentsLength)) * -1) + '%)');
                } else {

                    element.addStyle(subContent, 'transform', 'translateX(' + ((currentContent * (width / contentsLength)) * -1) + '%)');
                }
            } else {

                if (position === 'vertical') {

                    subContent.style.marginTop = ((currentContent * height) * -1) + '%';
                } else {

                    subContent.style.marginLeft = ((currentContent * width) * -1) + '%';
                }
            }

            if (window.addEventListener) {

                element.addEffect();
            }
        } else {

            currentContent = (contentsLength - 1);

            element.navigation(currentContent);
        }

        element.start();

        return element;
    };

    element.navigation         = function (value) {

        /*
        ** DATA
        *******/
        element.data();

        element.addTransition();

        element.stop();

        value = element.value || value;

        currentContent = parseInt(value, 10);

        if (subContent.style.transform !== undefined) {

            if (position === 'vertical') {

                element.addStyle(subContent, 'transform', 'translateY(' + ((currentContent * (height / contentsLength)) * -1) + '%)');
            } else {

                element.addStyle(subContent, 'transform', 'translateX(' + ((currentContent * (width / contentsLength)) * -1) + '%)');
            }
        } else {

            if (position === 'vertical') {

                subContent.style.marginTop = ((currentContent * height) * -1) + '%';
            } else {

                subContent.style.marginLeft = ((currentContent * width) * -1) + '%';
            }
        }

        if (window.addEventListener) {

            element.addEffect();
        }

        element.start();

        return false;
    };

    element.destroy            = function () {

        element.events(true);
        element.replaceWith(element.elementClones);
    };

    element.init();

    return element;
}));