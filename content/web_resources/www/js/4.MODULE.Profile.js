/*
** PROFILE
*******/
APP.Modules.Profile = (function (app, Private, Public) {

    'use strict';

    Public = {
        configuration : {
            ID     : 'Profile',
            NAME   : 'Profile',
            ENGAGE : true
        }
    };

    Public.init = function () {

        /*
        ** GET USER GAMES
        *******/
        Private.getGames();

        return true;
    };

    /*
    ** GET USER PSN ID
    *******/
    Public.getPsnId                = function () {

        var pattern     = /\/game\/[^\/]*\/(.*)/,
            pattern_2   = /\/profile\/(.*)/,
            pathname    = window.location.pathname;

        if (pattern.test(pathname)) {

            return pattern.exec(pathname)[1];
        } else if (pattern_2.test(pathname)) {

            return pattern_2.exec(pathname)[1];
        }

        return false;
    };

    /*
    ** GET USER GAMES
    *******/
    Private.getGames                = function (data) {

        data = Object.assign(data || {}, {
            psnId : (Public.getPsnId() || app.Modules.Session.psnId)
        });

        Private.gameList.find('.loader').show();

        $(window).off('scroll');

        $.ajax({
            url         : app.Modules.Api.games,
            type        : 'GET',
            dataType    : 'json', 
            data        : data,
            success     : Private.getGamesSuccess
        });
    };

    Private.getGamesSuccess         = function (data) {

        var status      = data.status,
            response    = data.response,
            dom         = document.createElement('ul'),
            template    = document.getElementById('games-list-template'),
            content     = document.getElementsByClassName('gamesList')[0].getElementsByClassName('list')[0];

        Private.gameList.find('.loader').hide();

        if (status) {

            app.Modules.General.curPage     = response.pagination.current;
            app.Modules.General.pageTotal   = response.pagination.total;

            dom.innerHTML = Mustache.render(template.innerHTML.trim(), {
                                games   : response.games,
                                psnId   : Public.getPsnId()
                            });

            content.appendChild(dom);

            /*
            ** INFINITE SCROLL
            *******/
            app.Modules.General.callback = Private.getGames;

            $(window).on('scroll', app.Modules.General.infiniteScroll);
        }
    };

    return Public;

}(APP, {}, {}));