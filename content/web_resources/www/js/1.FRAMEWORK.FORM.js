(function (global, factory) {

	global.Form = factory;
}(this, function factory (config) {

	'use strict';

    var form                = null,
        formSubmit          = null,
        formAction          = null,
        formMethod          = null,
        formTarget          = null,
        formMessage         = null,
        formAjax            = null,
        formCustomSubmit    = null,

        fields              = null,

        field               = null,
        fieldId             = null,
        fieldName           = null,
        fieldValue          = null,
        fieldLength         = null,
        fieldAttribute      = null,
        fieldType           = null,
        fieldMax            = null,
        fieldMin            = null,
        fieldDisabled       = null,
        fieldChecked        = null,
        fieldRequire        = null,
        fieldElements       = [],

        ul                  = null,
        li                  = null,
        error               = false;

	if (!(this instanceof factory)) {

		return new factory(config).init();
	}

    factory.prototype.init    					= function () {

        try {

        	self = this;

            if (this.instance()) {

                if (this.setAttributes()) {

                    this.setSubmit();
                }
            }
        } catch (err) {

            console.log(err);
        }
    };

    factory.prototype.instance                	= function () {

        if (config) {

            form        = this.getId(config.form);
            formSubmit  = this.getId(config.submit);

            formAction              = config.action;
            formMethod              = config.method;
            formTarget              = config.target;
            formAjax                = config.ajax;
            formCustomSubmit        = config.customSubmit;

            /*
            ** NÃO PERMITE QUE ESTANCIODOR CONTINUE CASO O FORMULARIO NAO EXISTA
            *******/
            if (!form) {

                return false;
            }

            /*
            ** NÃO PERMITE QUE O FORMULARIO SEJA INSTANCIADO MAIS DE UMA VEZ
            *******/
            if(form.getAttribute('data-init')) {

                return false;
            }

            /*
            ** SE NÃO EXISTIR UM DATA INIT, É ADICIONADO AO FORMULARIO
            *******/
            form.setAttribute('data-init', true);

            /*
            ** VERIFICANDO É NECESSARIO CONFIGURAR ACTION
            *******/
            if (formAction) {

                if (!form.getAttribute('action')) {

                    form.setAttribute('action', formAction);
                }
            }

            /*
            ** VERIFICANDO É NECESSARIO CONFIGURAR METHOD
            *******/
            if (formMethod) {

                if (!form.getAttribute('method')) {

                    form.setAttribute('method', formMethod);
                }
            }

            /*
            ** VERIFICANDO É NECESSARIO CONFIGURAR TARGET
            *******/
            if (formTarget) {

                if (!form.getAttribute('target')) {

                    form.setAttribute('target', formTarget);
                }
            }

            return true;
        }

        return false;
    };

    factory.prototype.setAttributes           	= function () {

    	try {

            /*
            ** CAMPOS DO FORMULARIO
            *******/
            fields  = config.fields;

            if (fields) {

                for (var index in fields) {

                    field       = fields[index];
                    fieldId     = form.elements[field.id];

                    fieldElements[field.id] = fieldId;

                    if (fieldId) {

                        if (fieldId.nodeType === 1) {

                            fieldValue      = fieldId.value;
                            fieldAttribute  = field.attributes;
                            fieldType       = field.type;

                            if (fieldAttribute) {

                                for (var attribute in fieldAttribute) {

                                    if (!fieldId.getAttribute(attribute)) {

                                        fieldId.setAttribute(attribute, fieldAttribute[attribute]);
                                    }
                                }

                                /*
                                ** VALIDANDO LIMITE DOS CAMPOS
                                *******/
                                if (fieldAttribute.min && fieldAttribute.max) {

                                    fieldId.addEventListener('keypress', UTILS.maxLimit, false);
                                    fieldId.addEventListener('paste', UTILS.maxLimit, false);
                                }
                            }

                            /*
                            ** VALIDANDO TIPAGEM DOS CAMPOS
                            *******/
                            if (fieldType) {

                                if (fieldType === 'numeric' || fieldType === 'cpf' || fieldType === 'date') {

                                    fieldId.addEventListener('keypress', UTILS.numericType, false);
                                    fieldId.addEventListener('paste', UTILS.numericType, false);
                                }
                            }
                        }
                    } else {

                        if (field.id) {

                            throw 'Campo "' + (field.id)  + '", não encontrado.';
                        }
                    }
                }
            }
        } catch (err) {

            return false;
        }

        return true;
    };

    factory.prototype.setValidation           	= function (config) {

    	/*
        ** CAMPOS DO FORMULARIO
        *******/
        fields  = config.fields;

        ul = document.createElement('ul');

        if (fields) {

            for (var index in fields) {

            	form        = this.getId(config.form);

                field       = fields[index];
                fieldId     = form.elements[field.id];
                fieldName   = form.elements[field.name];

                if (fieldId) {

                    if (fieldId.nodeType === 1) {

                        li = document.createElement('li');

                        fieldAttribute  = field.attributes;
                        fieldType       = field.type;

                        fieldRequire    = field.require;

                        fieldValue      = fieldId.value;
                        fieldDisabled   = fieldId.disabled;
                        fieldLength     = fieldValue.length;

                        /*
                        ** REMOVE CLASSE DE ERRO
                        *******/
                        fieldId.classList.remove('field-error');

                        /*
                        ** VALIDANDO CAMPOS VAZIOS
                        *******/
                        if (fieldLength <= 0 && !fieldDisabled && fieldRequire) {

                            fieldId.classList.add('field-error');

                            if (fieldId.nodeName === 'INPUT' || fieldId.nodeName === 'TEXTAREA') {

                                li.innerHTML = 'O campo ' + field.message + ' não pode estar vázio';
                            } else if (fieldId.nodeName === 'SELECT') {

                                li.innerHTML = 'O campo ' + field.message + ' não foi selecionado';
                            }

                            ul.appendChild(li);
                            
                            error = true;
                        }

                        /*
                        ** VALIDANDO TIPO DOS CAMPOS
                        *******/
                        if (fieldType && !error) {

                            if (fieldLength > 0 && !fieldDisabled) {

                                if (fieldType === 'numeric') {

                                    if (!UTILS.numericType({ value : fieldValue })) {

                                        fieldId.classList.add('field-error');

                                        li.innerHTML = 'O campo ' +  field.message + ' não está em um formato válido!';

                                        ul.appendChild(li);
                                        
                                        error = true;
                                    }
                                } else if (fieldType === 'email') {

                                    if (!UTILS.emailType({ value : fieldValue })) {

                                        fieldId.classList.add('field-error');

                                        li.innerHTML = 'O Email não está em um formato válido!';

                                        ul.appendChild(li);
                                        
                                        error = true;
                                    }
                                } else if (fieldType === 'cpf') {

                                    if (!UTILS.cpfType({ value : fieldValue })) {

                                        fieldId.classList.add('field-error');

                                        li.innerHTML = 'O CPF não está em um formato válido!';

                                        ul.appendChild(li);
                                        
                                        error = true;
                                    }
                                } else if (fieldType === 'date') {

                                    if (!UTILS.dateType({ value : fieldValue })) {

                                        fieldId.classList.add('field-error');

                                        li.innerHTML = 'A Data não está em um formato válido!';

                                        ul.appendChild(li);
                                        
                                        error = true;
                                    }
                                }
                            }
                        }

                        /*
                        ** VALIDANDO LIMITE DOS CAMPOS
                        *******/
                        if (fieldAttribute  && !error) {

                            fieldMax = fieldAttribute.max;
                            fieldMin = fieldAttribute.min;

                            if (fieldMin && fieldRequire) {

                                if (UTILS.minLimit({ value : fieldValue, min : fieldMin }) && !fieldDisabled) {

                                    fieldId.classList.add('field-error');

                                    li.innerHTML = 'Preencha o valor mínimo de ' + fieldMin + ' caracteres';

                                    ul.appendChild(li);
                                    
                                    error = true;
                                }
                            }

                            if (fieldMax && fieldRequire) {

                                if (UTILS.maxLimit({ value : fieldValue, max : fieldMax }) && !fieldDisabled) {

                                    fieldId.classList.add('field-error');

                                    li.innerHTML = 'O valor máximo de ' + fieldMax + ' caracteres foi ultrapassado';

                                    ul.appendChild(li);
                                    
                                    error = true;
                                }
                            }
                        }
                    }
                } else if (fieldName) {

                	if (fieldName.type === 'checkbox' || fieldName.type === 'radio') {

	                    li = document.createElement('li');

	                    for (var index2 in fieldName) {

	                        if (fieldName[index2].nodeType === 1) {

	                            fieldChecked = fieldName[index2].checked;

	                            if (fieldChecked) {

	                                break;
	                            }
	                        }
	                    }

	                    if (!fieldChecked) {

	                        li.innerHTML = 'O campo ' + field.message + ' não foi selecionado';

	                        ul.appendChild(li);
                            
	                        error = true;
	                    }
                	}
                }
            }
        }

        form.lastChild.classList.remove('error');

        form.lastChild.innerHTML        = ul.outerHTML;
        form.lastChild.style.display    = 'none';

        if (error) {

            form.lastChild.classList.add('error');

            form.lastChild.innerHTML        = ul.outerHTML;
            form.lastChild.style.display    = 'block';

            error = false;

            return false;
        }

        return true;
    };

    factory.prototype.setAdditionalValidation 	= function (config) {

        var result                  = true,
            additionalValidation    = config.additionalValidation;

        if (typeof additionalValidation === 'object') {

            for (var index in additionalValidation) {

                if (!additionalValidation[index](form, fieldElements)) {

                    result  = false;

                    break;
                }
            }
        } else if (typeof additionalValidation === 'function') {

            return additionalValidation(form, fieldElements);
        }

        return result;
    };

    factory.prototype.setSubmit               	= function (event) {

        formMessage    = document.createElement('div');
        formMessage.id = 'formMessage';

        formMessage.style.display = 'none';

        form.appendChild(formMessage);

        /*
        ** ENVIANDO FORMULARIO
        *******/
        if (!formSubmit) {

            throw 'Botão para "submit" não encontrado.';
        }

        formSubmit.addEventListener('click', this.submit, false);
        formSubmit.config = config;
    };

    factory.prototype.submit                  	= function (event) {

        var formData    = UTILS.serialize({
                            form      : config.form,
                            json      : true,
                            stringify : false
                        });

        if (self.setValidation(config)) {

            if (!self.setAdditionalValidation(config)) {

                return event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }

            if (formAjax) {

                if (formAjax.actived) {

                    /*
                    ** NAO UTILIZAR URL, NO PARAMETROS DA CONFIGURAÇAO DO AJAX
                    ** CASO HAJA O PARAMETRO URL, ELE SERÁ SUBSTITUIDO PELA VARIAVEL "formAction"
                    *******/
                    formAjax.config.url = (formAjax.config.url || formAction);


                    /** NAO UTILIZAR TYPE, NO PARAMETROS DA CONFIGURAÇAO DO AJAX
                    ** CASO HAJA O PARAMETRO TYPE, ELE SERÁ SUBSTITUIDO PELA VARIAVEL "formMethod"
                    ******/
                    formAjax.config.type = (formAjax.config.type || formMethod);

                    formAjax.config.data = formData;

                    /*
                    ** ENVIANDO FORMULARIO VIA AJAX
                    *******/
                    $.ajax(formAjax.config);
                } else {

                    /*
                    ** ENVIANDO FORMULARIO SEM O USO DO AJAX
                    *******/
                    form.submit();
                }
            } else {

                if (formCustomSubmit) {

                    formCustomSubmit(formData);
                } else {

                    form.submit();
                }
            }
        }

        return event.preventDefault ? event.preventDefault() : event.returnValue = false;
    };

    factory.prototype.getId						= function (id) {

        return document.getElementById(id);
    };

    factory.prototype.getName 					= function (name) {

        return document.getElementsByName(name);
    };
}));