<?php
	
	session_start();

	date_default_timezone_set('America/Sao_Paulo');

	error_reporting(E_ALL);

	set_time_limit(0);

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('memory_limit', '256M');
	ini_set('max_execution_time', 10000);
	ini_set('mysql.connect_timeout', 10000);
	ini_set('default_socket_timeout', 10000); 
	ini_set('mysql.reconnect', 1);
	ini_set('mysql.allow_persistent', 1);

	/*
	** CARREGANDO CONTROLLERS
	*******/
	$folder			= null;
	$arrDirectory 	= array();
	$directory 		= array(
		'framework',
		'controllers'
	);

	foreach ($directory as $dir) {

		$folder = dirname(__FILE__) . '/' . $dir . '/';
		
		if (is_dir($folder)) {

			foreach ($arrDirectory = scandir($folder) as $value) {
				
				$file = $folder . $value;
				
				if (is_file($file) && substr($file, -4) === '.php') {

					include_once $file;
				}
			}
		}
	}

	new Init();

	/*
	** EXECUÇOES EM SEGUNDO PLANO
	*******/
	if (@sizeof($argv) > 0) {

		$class 	= isset($argv[1]) ? $argv[1] : null;
		$method = isset($argv[2]) ? $argv[2] : null;

		switch ($class) {

			case 'Profile':
				
				if ($method === 'execUpdate') {

					$class::$method($argv[3], $argv[4]);
				}
				break;

			case 'AdmPsnUpdate':
				
				if ($method === 'requestUpdate') {

					$class::$method($argv[3], $argv[4]);
				}
				break;

			case 'WebSocket':
				
				if ($method === 'connect') {

					$class::$method();
				}
				break;
			
			default:
				break;
		}
	}

?>