<?php
	
	class Trophies {

		function __construct() {

			Route::setList(array(
				array(
					'method' 	=> 'GET',
					'route' 	=> 'trophies',
					'function' 	=> 'getTrophies'
				),
				array(
					'method' 	=> 'GET',
					'route' 	=> 'details',
					'function' 	=> 'getDetails'
				)
			));
		}

		public static function getTrophies() {

			$status 	= false;
			$message 	= false;
			$error 		= false;
			$response 	= false;

			/*
			** QUERY SELECT
			*******/
			$querySelect = array(
				'
					SELECT 
						gam.ga_id,
						gam.ga_npcommunicationid,
					    gam.ga_name,
					    gam.ga_platform,
					    gam.ga_platinum,
					    gam.ga_gold,
					    gam.ga_silver,
					    gam.ga_bronze,
					    ggp.gg_groupname,
					    ggp.gg_platinum,
					    ggp.gg_gold,
					    ggp.gg_silver,
					    ggp.gg_bronze,
					    trf.tr_id,
					    trf.gg_groupid,
					    trf.tr_trophyid,
					    trf.tr_trophytype,
					    trf.tr_trophyname,
					    trf.tr_trophydetail,
					    (
							SELECT 
								COUNT(trk.tr_id)
							FROM 
								ps_tricks AS trk 
							WHERE 
								trk.tr_id = trf.tr_id
					    ) as tricks
					FROM 
						ps_games AS gam 
					    INNER JOIN ps_trophies 		AS trf ON gam.ga_npcommunicationid = trf.ga_npcommunicationid 
						LEFT JOIN ps_games_group 	AS ggp ON gam.ga_npcommunicationid = ggp.ga_npcommunicationid AND trf.gg_groupid = ggp.gg_groupid
					WHERE 
						ga_id = ? 
					GROUP BY 
						trf.tr_trophyid   
					ORDER BY 
						trf.tr_trophyid ASC
				',
				'
					SELECT  
						usr.us_id,
						usr.psn_id, 
						prf.pr_plus,
						prf.pr_region,
						usr.us_firstname,
					    usg.ug_progress,
					    usg.ug_platinum,
					    usg.ug_gold,
					    usg.ug_silver,
					    usg.ug_bronze,
						utr.ut_trophyearned, 
						utr.ut_trophyearneddate,
						gam.ga_id,
						gam.ga_npcommunicationid,
						gam.ga_name,
						gam.ga_platform,
						gam.ga_platinum,
						gam.ga_gold,
						gam.ga_silver,
						gam.ga_bronze,
						ggp.gg_groupname,
						ggp.gg_platinum,
						ggp.gg_gold,
						ggp.gg_silver,
						ggp.gg_bronze,
						trf.tr_id,
						trf.gg_groupid,
						trf.tr_trophyid,
						trf.tr_trophytype,
						trf.tr_trophyname,
						trf.tr_trophydetail,
					    (
							SELECT 
								COUNT(trk.tr_id)
							FROM 
								ps_tricks AS trk 
							WHERE 
								trk.tr_id = trf.tr_id
					    ) as tricks
					FROM 
						ps_games AS gam 
						INNER JOIN ps_trophies 		AS trf ON gam.ga_npcommunicationid 	= trf.ga_npcommunicationid 
                        LEFT JOIN ps_games_group 	AS ggp ON gam.ga_npcommunicationid	= ggp.ga_npcommunicationid AND trf.gg_groupid = ggp.gg_groupid 
					    INNER JOIN ps_user_games 	AS usg ON gam.ga_npcommunicationid 	= usg.ga_npcommunicationid 
					    INNER JOIN ps_user 			AS usr ON usr.us_id = usg.us_id 
					    INNER JOIN ps_profile 		AS prf ON prf.us_id = usr.us_id 
					    LEFT JOIN ps_user_trophies	AS utr ON utr.ga_npcommunicationid	= trf.ga_npcommunicationid AND trf.tr_trophyid = utr.tr_trophyid AND utr.us_id = usr.us_id 
					WHERE 
						usr.psn_id = ? AND gam.ga_id = ?
					GROUP BY 
						trf.tr_trophyid   
					ORDER BY 
						trf.tr_trophyid ASC
				',
				'
					SELECT 
						ggp.gg_groupid,
					    SUM(CASE WHEN trf.tr_trophytype = "platinum" 	AND ut_trophyearned = 1 THEN 1 ELSE 0 END) as platinum,
						SUM(CASE WHEN trf.tr_trophytype = "gold" 		AND ut_trophyearned = 1 THEN 1 ELSE 0 END) as gold,
					    SUM(CASE WHEN trf.tr_trophytype = "silver" 		AND ut_trophyearned = 1 THEN 1 ELSE 0 END) as silver,
					    SUM(CASE WHEN trf.tr_trophytype = "bronze" 		AND ut_trophyearned = 1 THEN 1 ELSE 0 END) as bronze
					FROM 
						ps_games AS gam 
						INNER JOIN ps_trophies 		AS trf ON gam.ga_npcommunicationid 	= trf.ga_npcommunicationid 
						LEFT JOIN ps_games_group 	AS ggp ON gam.ga_npcommunicationid	= ggp.ga_npcommunicationid AND trf.gg_groupid = ggp.gg_groupid 
						LEFT JOIN ps_user 			AS usr ON usr.psn_id = ?
						LEFT JOIN ps_user_trophies	AS utr ON utr.ga_npcommunicationid	= trf.ga_npcommunicationid AND trf.tr_trophyid = utr.tr_trophyid AND utr.us_id = usr.us_id
					WHERE 
						ga_id = ? 
					GROUP BY 
						ggp.gg_groupid 
					ORDER BY 
						ggp.gg_groupid 
				'
			);

			/*
			** REQUEST FORM
			*******/
			$formData = Utils::getRequest();

			if (isset($formData)) {

				/*
				** GAME ID
				*******/
				if (isset($formData -> gaId)) {

					if (isset($formData -> psnId)) {

						$result		= Connect::query('select', $querySelect[1], array($formData -> psnId, $formData -> gaId));
						$result_2 	= Connect::query('select', $querySelect[2], array($formData -> psnId, $formData -> gaId));

						if (!count($result)) {

							$result = Connect::query('select', $querySelect[0], array($formData -> gaId));
						}
					} else {

						$result = Connect::query('select', $querySelect[0], array($formData -> gaId));
					}

					/*
					** DETALHES DO JOGO
					*******/
					if (count($result)) {

						$status 	= true;
						$message	= 'Detalhes listados com sucesso';

						$usId 		= @$result[0] -> us_id;
						$psnId 		= @$result[0] -> psn_id;
						$plus 		= @(bool)$result[0] -> pr_plus;

						$id 		= $result[0] -> ga_id;
						$npid 		= $result[0] -> ga_npcommunicationid;
						$name 		= $result[0] -> ga_name;

						$avatar 	= '/images/games/' . $npid . '/game/' . $npid . '.png';

						$platform 	= $result[0] -> ga_platform;
						$platform 	= explode(',', strtolower($platform));

						if (!empty($psnId)) {

							$response['profile'] = array(
								'usId'	=> $usId,
								'name'	=> $result[0] -> us_firstname
							);

							$response['psn'] = array(
								'avatar'	=> '/images/users/' . $usId . '/' . strtolower($psnId) . '.png',
								'psnId'		=> $psnId,
								'plus' 		=> $plus,
								'region' 	=> $result[0] -> pr_region,
								'summary' 	=> array(
									'progress'	=> $result[0] -> ug_progress,
									'platinum'	=> $result[0] -> ug_platinum,
									'gold'		=> $result[0] -> ug_gold,
									'silver'	=> $result[0] -> ug_silver,
									'bronze'	=> $result[0] -> ug_bronze
								)
							);
						}
						
						$response['game'] = array(
							'id' 		=> $id,
							'npid' 		=> $npid,
							'name' 		=> $name,
							'platform' 	=> $platform,
							'summary' 	=> array(
								'platinum'	=> $result[0] -> ga_platinum,
								'gold' 		=> $result[0] -> ga_gold,
								'silver'	=> $result[0] -> ga_silver,
								'bronze'	=> $result[0] -> ga_bronze,
							),
							'avatar' 	=> $avatar
						);

						foreach ($result as $trophy) {

							$trId  			= $trophy -> tr_id;
							$groupid  		= $trophy -> gg_groupid;
							$groupname		= $trophy -> gg_groupname;
							$groupavatar	= '/images/games/' . $npid . '/group/' . $groupid . '/' . $npid . '.png';

							$trophyid 		= $trophy -> tr_trophyid;
							$trophyavatar 	= '/images/games/' . $npid . '/trophies/' . $trophyid . '.png';

							$platinum 		= $groupid 	? $trophy -> gg_platinum 	: $result[0] -> ga_platinum;
							$gold 			= $groupid 	? $trophy -> gg_gold 		: $result[0] -> ga_gold;
							$silver 		= $groupid 	? $trophy -> gg_silver 		: $result[0] -> ga_silver;
							$bronze 		= $groupid 	? $trophy -> gg_bronze 		: $result[0] -> ga_bronze;

							if (!isset($response['groups'][$groupid])) {

								if (isset($result_2)) {

									foreach ($result_2 as $earned) {

										if ($earned -> gg_groupid === $groupid) {

											$userPlatinum 	= (int)$earned -> platinum;
											$userGold 		= (int)$earned -> gold;
											$userSilver 	= (int)$earned -> silver;
											$userBronze 	= (int)$earned -> bronze;

											break;
										}
									}
								}

								$response['groups'][$groupid] = array(
									'groupid' 	=> (!$groupid ? 'default' : $groupid),
									'name' 		=> (!$groupname ? $name : $groupname),
									'avatar'	=> (!$groupid ? $avatar : $groupavatar),
									'summary' 	=> array(
										'game' => array(
											'platinum'	=> $platinum,
											'gold' 		=> $gold,
											'silver'	=> $silver,
											'bronze'	=> $bronze
										),
										'user' => array(
											'platinum' 	=> @$userPlatinum, 
											'gold' 		=> @$userGold,
											'silver' 	=> @$userSilver,
											'bronze' 	=> @$userBronze
										),
										'progress' 	=> Psn::gameProgress(array(
											'game' => array(
												'gold' 		=> $gold,
												'silver' 	=> $silver,
												'bronze' 	=> $bronze
											),
											'user' => array(
												'gold' 		=> @$userGold,
												'silver' 	=> @$userSilver,
												'bronze' 	=> @$userBronze
											)
										))
									)
								);
							}

							$response['groups'][$groupid]['allEarned'] = ((int)$response['groups'][$groupid]['summary']['progress'] === 100);

							$response['groups'][$groupid]['trophies'][] = array(
								'id' 			=> $trId,
								'trophyid' 		=> $trophyid,
								'avatar'		=> $trophyavatar,
								'type' 			=> $trophy -> tr_trophytype,
								'name' 			=> $trophy -> tr_trophyname,
								'details' 		=> $trophy -> tr_trophydetail,
								'earned' 		=> @$trophy -> ut_trophyearned,
								'earneddate'	=> @$trophy -> ut_trophyearneddate ? @date_format(date_create($trophy -> ut_trophyearneddate), "d/m/Y H:i:s") : null,
								'tricks' 		=> $trophy -> tricks
							);
						}
					}
				}
			}

			return Utils::response($status, $message, $error, $response);
		}

		public static function getDetails() {

			$status 	= false;
			$message 	= false;
			$error 		= false;
			$response 	= false;

			/*
			** QUERY SELECT
			*******/
			$querySelect = array(
				'
					SELECT 
						trf.tr_id,
						trf.ga_npcommunicationid,
						trf.tr_trophyid,
						trf.tr_trophytype,
						trf.tr_trophyname,
						trf.tr_trophydetail,
					    gam.ga_id,
					    gam.ga_name
					FROM 
						ps_trophies AS trf 
					    INNER JOIN ps_games AS gam ON trf.ga_npcommunicationid = gam.ga_npcommunicationid
					WHERE 
						tr_id = ?
				'
			);

			/*
			** SESSION DATA
			*******/
			$session = Session::getSession();

			/*
			** REQUEST FORM
			*******/
			$formData = Utils::getRequest();

			if (count($formData)) {

				if (isset($formData -> tr_id)) {

					$trid = $formData -> tr_id;

					if ($result = Connect::query('select', $querySelect[0], array($trid))) {

						$status 	= true;
						$message 	= 'Troféu listado com sucesso';

						$tricks  	= json_decode(Tricks::getTricks());

						$id				= $result[0] -> tr_id;
						$npid 			= $result[0] -> ga_npcommunicationid;	
						$trophyid 		= $result[0] -> tr_trophyid;
						$trophyavatar 	= '/images/games/' . $npid . '/trophies/' . $trophyid . '.png';

						
						$link 		= strtolower($result[0] -> ga_id . ' ' . trim($result[0] -> ga_name));
						$link 		= str_replace(' ', '-', $link);
						$link 		= preg_replace('/(?!\-)\W/', '', $link);

						$response['trophy'] = array(
							'id' 		=> $id,
							'trophyid' 	=> $trophyid,
							'avatar'	=> $trophyavatar,
							'npid'		=> $npid,
							'type' 		=> $result[0] -> tr_trophytype,
							'name' 		=> $result[0] -> tr_trophyname,
							'details' 	=> $result[0] -> tr_trophydetail,
							'link' 		=> $link
						);

						if ($tricks -> status) {

							$response['tricks'] = $tricks -> response -> tricks;
						}
					}
				}
			}

			return Utils::response($status, $message, $error, $response);
		}
	}

?>