<?php
    
    class Connect {

        private static $instance    = null;
        private static $config      = null;

        private static function init() {

            $local = false;

             self::$config   = array(
                'DB_SERVER'     => 'localhost',
                'DB_USER'       => 'my_user',
                'DB_PASSWORD'   => 'my_pass',
                'DB_NAME'       => 'my_db'
            );

            self::$instance = new mysqli(self::$config['DB_SERVER'], self::$config['DB_USER'], self::$config['DB_PASSWORD'], self::$config['DB_NAME']);
            self::$instance -> set_charset("utf8");

            if (!mysqli_connect_errno()) {

                return self::$instance;
            }

            echo 'Erro ao conectar : ' . mysqli_connect_error();

            return false;
        }

        public static function query($type, $query, $params = null) {

            $return = null;
            $result = null;
        
            /*
            ** INICIANDO CONEXAO
            *******/
            $init = self::init();

            if ($type === 'insert' OR $type === 'update' OR $type === 'delete') {

                /*
                ** PREPARANDO A QUERY
                *******/
                $instance = $init -> prepare($query) or die($init -> error);

                /*
                ** PARAMETROS A SEREM USADOS NA QUERY E RETORNANDO RESULTADO DA MESMA
                *******/
                $result = self::prepareBindParams($params, $instance);

                /*
                ** SE NAO HOUVER ERROS
                *******/
                if (!$instance -> errno) {

                    /*
                    ** UPDATE / INSERT / DELETE
                    *******/
                    if (($type === 'update' OR $type === 'delete') && $instance -> affected_rows) {

                        $return = true;
                    } else if ($type === 'insert' && $instance -> insert_id) {

                        $return = $instance -> insert_id;
                    }
                }

                $instance -> close();
                $init -> close();

            } else if ($type === 'select' OR $type === 'procedure') {

                if (sizeof($params) > 0) {

                    /*
                    ** PREPARANDO A QUERY
                    *******/
                    $instance = $init -> prepare($query) or die($init -> error);

                    /*
                    ** PARAMETROS A SEREM USADOS NA QUERY E RETORNANDO RESULTADO DA MESMA
                    *******/
                    $instance = self::prepareBindParams($params, $instance);

                    $metadata = $instance -> result_metadata();

                    while ($field = $metadata -> fetch_field()) {

                        $parameters[] = &$row[$field -> name];
                    }

                    @call_user_func_array(array($instance, 'bind_result'), $parameters);

                    if (!$instance -> errno) {

                        while ($instance -> fetch()) {

                            foreach($row as $key => $val) {

                                $x[$key] = $val;
                            }

                            $return[] = $x;
                        }

                        $return = json_decode(json_encode($return));
                    }

                    $instance -> close();
                } else {

                    /*
                    ** PREPARANDO A QUERY
                    *******/
                    $result = $init -> query($query) or die($init -> error);

                    if ($result -> num_rows) {

                        while ($row = $result -> fetch_assoc()) {

                            $return[] = $row;
                        }

                        $return = json_decode(json_encode($return));
                    }

                    $result -> close();
                    
                    $init -> close();
                }
            }

            return $return;
        }

        private static function prepareBindParams($params, $instance) {

            $bindTypes  = '';
            $bindParams = array();
            $parameters = array();
            $bindName   = null;

            if (sizeof($params) > 0) {

                /*
                ** TIPO DOS PARAMETROS
                *******/
                foreach($params as $param) {

                    $bindTypes .= self::getType($param);
                }

                $bindParams[]   = $bindTypes;

                /*
                ** VALORES DOS PARAMETROS
                *******/
                foreach($params as $param) {

                    $bindName       = $param;
                    $$bindName      = $param;
                    $bindParams[]   = &$$bindName;
                }
                
                @call_user_func_array(array($instance, 'bind_param'), $bindParams);
            }

            $instance -> execute();

            return $instance;
        }

        private static function getType($value) {

            switch (gettype($value)) {

                case 'NULL':
                case 'string':
                    return 's';
                    break;

                case 'integer':
                    return 'i';
                    break;

                case 'blob':
                    return 'b';
                    break;

                case 'double':
                    return 'd';
                    break;
            }

            return;
        }
    }

?>