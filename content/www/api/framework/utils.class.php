<?php
	
	class Utils {

        private static $request = false;

        public static function response ($status, $message, $errors, $response) {

            $error = array();

            if (is_array($errors)) {

                foreach ($errors as $err) {
                
                    $error[] = $err;
                }
            } else {

                $error = $errors;
            }

            return json_encode(array(
                'status'    => $status,
                'message'   => $message,
                'error'     => $error,
                'response'  => $response
            ));
        }

		public static function getMethod() {

			return isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : false;
		}

        public static function serialize($array) {

            if (count($array)) {

                $fieldsString = '';

                foreach($array as $key => $value) { 

                    $fieldsString .= $key . '=' . $value . '&'; 
                }

                return rtrim($fieldsString, '&');
            }

            return false;
        }

		public static function requestCurl($config) {

            $curl       = curl_init();

            $options    = array(
                CURLOPT_URL             => $config['url'],
                CURLOPT_CUSTOMREQUEST   => $config['method'],
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                //CURLOPT_HEADER          => isset($config['h1']) ? $config['h1'] : false,
                //CURLINFO_HEADER_OUT     => isset($config['h2']) ? $config['h2'] : false,  
                CURLOPT_SSL_VERIFYHOST  => 2,
                CURLOPT_HTTPHEADER      => isset($config['header']) ? $config['header'] : array(),
                CURLOPT_COOKIEFILE      => isset($config['getCookie']) ? dirname(dirname(__FILE__)) . '/cookie/' . $config['getCookie'] : false,
                CURLOPT_COOKIEJAR       => isset($config['setCookie']) ? dirname(dirname(__FILE__)) . '/cookie/' . $config['setCookie'] : false,
                CURLOPT_CONNECTTIMEOUT  => 0,
                CURLOPT_TIMEOUT         => 10000
            );

            foreach ($options as $key => $value) {
                
                @curl_setopt($curl, $key, $value);
            }

            if (count(@$config['postData'])) {

                $options = array(
                    CURLOPT_POST            => count(@$config['postData']),
                    CURLOPT_POSTFIELDS      => @$config['postData']
                );

                foreach ($options as $key => $value) {
                
                    @curl_setopt($curl, $key, $value);
                }
            }

            $body       = curl_exec($curl);
            $response   = curl_getinfo($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);

            return array(
                'statusCode'    => $statusCode,
                'response'      => $response,
                'body'          => $body
            );
		}

        public static function requestMultiCurl($config) {

            $curl       = curl_init();

            $options    = array(
                CURLOPT_URL             => $config['url'],
                CURLOPT_CUSTOMREQUEST   => $config['method'],
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                //CURLOPT_HEADER          => true, //
                //CURLINFO_HEADER_OUT     => true, //
                CURLOPT_SSL_VERIFYHOST  => 2,
                CURLOPT_HTTPHEADER      => isset($config['header']) ? $config['header'] : array(),
                CURLOPT_COOKIEFILE      => isset($config['getCookie']) ? dirname(dirname(__FILE__)) . '/cookie/' . $config['getCookie'] : false,
                CURLOPT_COOKIEJAR       => isset($config['setCookie']) ? dirname(dirname(__FILE__)) . '/cookie/' . $config['setCookie'] : false,
                CURLOPT_CONNECTTIMEOUT  => 0,
                CURLOPT_TIMEOUT         => 10000
            );

            foreach ($options as $key => $value) {
                
                @curl_setopt($curl, $key, $value);
            }

            if (count(@$config['postData'])) {

                $options = array(
                    CURLOPT_POST            => count(@$config['postData']),
                    CURLOPT_POSTFIELDS      => @$config['postData']
                );

                foreach ($options as $key => $value) {
                
                    @curl_setopt($curl, $key, $value);
                }
            }

            return $curl;
        }

        public static function getRequest() {

            $request = false;

            if (!empty(self::$request)) {

                return self::$request;
            }

            switch (Utils::getMethod()) {

                case 'POST':
                case 'PUT':
                case 'DELETE':

                    $data   = self::getData();
                    $request = sizeof($data) ? $data : json_decode(json_encode($_POST));
                    break;

                case 'GET':
                    $request = json_decode(json_encode($_GET));
                    break;

                default:
                    break;
            }

            self::$request = $request;

            return $request;
        }

        private static function getData() {

            $return     = array();

            $input      = file_get_contents('php://input');
            $input      = urldecode($input);

            $boundary   = substr($input, 0, strpos($input, "\r\n"));

            if(!empty($boundary) && !json_decode($input)) {

                $fields = array_slice(explode($boundary, $input), 1);

                foreach ($fields as $field) {

                    if ($field === "--\r\n") {

                        break;
                    }

                    $field = ltrim($field);
                    $field = rtrim($field);
                    $field = preg_replace('/\r\n(.*)\r\n(.*)/mi', ' value="$2"', $field);

                    if (!empty($field)) {

                        preg_match('/name="([^"]+)"/mi', $field, $names);
                        preg_match('/value="([^"]+)"/mi', $field, $values);

                        @$return[trim($names[1])] = trim($values[1]);
                    }
                }
            }

            if (!sizeof($return)) {

                if (!json_decode($input)) {

                    parse_str($input, $return);
                } else {

                    $return = (array)json_decode($input);
                }
            }

            return json_decode(json_encode($return));
        }

        public static function execCommand($command) {

            if (substr(php_uname(), 0, 7) == "Windows") { 

                return pclose(popen("start /B ". $command, "r"));  
            } else { 

                return shell_exec($command);   
            } 
        }

        public static function dirExists($directory, $create = false) {

            if (isset($directory)) {

                $base       = dirname(dirname(__DIR__));
                $directory  = $base . '/' . ltrim($directory, '/');

                if (is_dir($directory)) {

                    return true;
                }

                if ($create) {

                    mkdir($directory);

                    return true;
                }
            }

            return false;
        }

        public static function fileExists($file) {

            if (isset($file)) {

                $base = dirname(dirname(__DIR__));
                $file = $base . '/' . ltrim($file, '/');

                if (file_exists($file)) {

                    return true;
                }
            }

            return false;
        }

        public static function downloadImage($oldImage, $newImage) {

            $base       = dirname(dirname(__DIR__));
            $newImage   = $base . '/' . ltrim($newImage, '/');

            $parseUrl   = parse_url($oldImage);
            $protocol   = !isset($parseUrl['scheme']) ? 'http:' : '';

            if (file_put_contents($newImage, file_get_contents($protocol . $oldImage))) {

                return true;
            }

            return false;
        }

        public static function isLocalhost() {

            $local = true;

            if (isset($_SERVER['SERVER_ADDR'])) {

                return in_array($_SERVER['SERVER_ADDR'], array('127.0.0.1', '::1', '192.168.1.45'));
            }

            return $local;
        }

        public static function scanDir($directory, $subFolders = false, $onlyFiles = false) {

            $base   = dirname(dirname(__DIR__));
            $dir    = $base . '/' . ltrim($directory, '/');

            $folders    = scandir($dir);

            $result     = array();

            foreach ($folders as $key => $file) {
                    
                if ($file !== '.' && $file !== '..') {

                    if (!$onlyFiles) {

                        if (is_dir($dir . $file)) {

                            $result[] = $directory . $file;

                            if ($subFolders) {

                                $result = array_merge($result, self::scanDir($directory . $file . '/', $subFolders, $onlyFiles));
                            }
                        }
                    } else {

                        if (is_file($dir . $file)) {

                            $result[] = $directory . $file;
                        } else if (is_dir($dir . $file)) {

                            $result = array_merge($result, self::scanDir($directory . $file . '/', $subFolders, $onlyFiles));
                        }
                    }
                }
            }

            return $result;
        }
	}

?>