<?php
	
	class Route {

		private static $controller;
		private static $route;
		private static $list;
		private static $instance;

		function __construct() {

			if (extract($_GET)) {

				if (class_exists(ucfirst($controller))) {

					self::$controller 	= ucfirst($controller);
					self::$route 		= empty($route) ? strtolower($controller) : $route;

					self::$instance 	= new $controller();

					self::init();
				}
			}
		}

		private static function init() {

			if ($list = self::getList()) {

				$controller = self::$controller;

				foreach ($list[$controller] as $config) {

					$method 	= strtoupper($config['method']);
					$route 		= $config['route'];
					$function	= $config['function'];

					if (self::$route === $route) {

						if (Utils::getMethod() === $method) {

							if (method_exists(self::$instance, $function)) {
								
								echo $controller::$function();

								break;
							}
						}
					}
				}
			}
		}

		public static function setList($list) {

			return self::$list[self::$controller] = $list;
		}

		private static function getList() {

			return self::$list;
		}
	}

?>