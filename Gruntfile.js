module.exports = function (grunt) {
    
    'use strict';

    var gruntConfig = {

        prefix      : '',

        srcPath     : 'content/web_resources/',
        destPath    : 'content/',

        sass        : {
            dist : {
                options : {
                    sourcemap   : 'none',
                    noCache     : true,
                    style       : 'compressed'
                },
                files : {
                    '<%= destPath + grunt.task.current.args[0] %>/css/<%= prefix %>framework.min.css' : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %>framework.min.scss',
                    '<%= destPath + grunt.task.current.args[0] %>/css/<%= prefix %>general.min.css' : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %>general.min.scss'
                }
            },
            subfolder : {
                options : {
                    sourcemap   : 'none',
                    noCache     : true,
                    style       : 'compressed'
                },
                files : {
                    '<%= destPath %>www/<%= grunt.task.current.args[0] %>/css/<%= prefix %><%= grunt.task.current.args[0] %>.min.css' : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %><%= grunt.task.current.args[0] %>.min.scss'
                }
            }
        },
        concat      : {
            framework   : {
                src : [
                    '<%= srcPath + grunt.task.current.args[0] %>/sass/src/0.*.scss'
                ],
                dest : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %>framework.min.scss',
            },
            general     : {
                src : [
                    '<%= srcPath + grunt.task.current.args[0] %>/sass/src/*.scss',
                    '!<%= srcPath + grunt.task.current.args[0] %>/sass/src/0.*.scss'
                ],
                dest : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %>general.min.scss',
            },
            subfolder   : {
                src : [
                    '<%= srcPath + grunt.task.current.args[0] %>/sass/src/*.scss',
                ],
                dest : '<%= srcPath + grunt.task.current.args[0] %>/sass/dest/<%= prefix %><%= grunt.task.current.args[0] %>.min.scss',
            }
        },
        uglify      : {
            plugin : {
                files: {
                    '<%= destPath + grunt.task.current.args[0] %>/js/<%= prefix %>plugin.min.js' : [ '<%= srcPath + grunt.task.current.args[0] %>/js/0.*.js' ]
                }
            },
            framework : {
                files: {
                    '<%= destPath + grunt.task.current.args[0] %>/js/<%= prefix %>framework.min.js' : [ '<%= srcPath + grunt.task.current.args[0] %>/js/1.*.js' ]
                }
            },
            widget : {
                files: {
                    '<%= destPath + grunt.task.current.args[0] %>/js/<%= prefix %>widget.min.js' : [ '<%= srcPath + grunt.task.current.args[0] %>/js/2.*.js' ]
                }
            },
            general : {
                files: {
                    '<%= destPath + grunt.task.current.args[0] %>/js/<%= prefix %>general.min.js' : [ '<%= srcPath + grunt.task.current.args[0] %>/js/3.*.js', '<%= srcPath + grunt.task.current.args[0] %>/js/4.*.js', '<%= srcPath + grunt.task.current.args[0] %>/js/MainApplication.js' ]
                }
            },
            subfolder : {
                files: {
                    '<%= destPath %>www/<%= grunt.task.current.args[0] %>/js/<%= prefix %><%= grunt.task.current.args[0] %>.min.js' : [ '<%= srcPath + grunt.task.current.args[0] %>/js/*.js' ]
                }
            }
        },
        jshint      : {
            files: [
                '<%= srcPath + grunt.task.current.args[0] %>/js/*.js',
                '!<%= srcPath + grunt.task.current.args[0] %>/js/0.*.js'
            ],
            subfolder : [
                '<%= srcPath + grunt.task.current.args[0] %>/js/*.js'
            ],
            options: {
                nonew   : true,
                newcap  : false,
                curly   : true,
                eqeqeq  : true,
                eqnull  : true,
                browser : true,
                globals : {
                    jQuery: true
                }
            }
        },
        includes    : {
            dist : {
                options : {
                    filenameSuffix : '.html',
                    includePath    : [
                        '<%= srcPath %><%= grunt.task.current.args[0] %>/templates/includes',
                        '<%= srcPath %><%= grunt.task.current.args[0] %>/templates/templates'
                    ],
                    duplicates     : true,
                    debug          : false
                },
                files: [{
                    cwd  : '<%= srcPath %><%= (grunt.task.current.args[0] != "www" ? grunt.task.current.args[0] : "www") %>/templates/',
                    src  : '*.html',
                    dest : '<%= destPath %>www/<%= (grunt.task.current.args[0] != "www" ? grunt.task.current.args[0] : "") %>'
                }]
            }
        },
        concurrent  : {
            options: {
                logConcurrentOutput: true
            },
            project : [
                'watch:css:<%= (grunt.task.current.args.join(":")) %>', 
                'watch:js:<%= (grunt.task.current.args.join(":")) %>',
                'watch:html:<%= (grunt.task.current.args.join(":")) %>'
            ]
        },
        watch       : {
            css : {
                files: [

                    /*
                    ** SASS
                    *******/
                    '<%= srcPath + grunt.task.current.args[1] %>/sass/src/*.scss'
                ],
                tasks: [

                    /*
                    ** SASS
                    *******/
                    'css:<%= grunt.task.current.args[1] %>'
                ]
            },
            html : {
                files: [

                    /*
                    ** HTML
                    *******/
                    '<%= srcPath + grunt.task.current.args[1] %>/templates/*.html',
                    '<%= srcPath + grunt.task.current.args[1] %>/templates/**/*.html',
                ],
                tasks: [
                    
                    /*
                    ** HTML
                    *******/
                    'html:<%= grunt.task.current.args[1] %>'
                ]
            },
            js : {
                files: [

                    /*
                    ** JS
                    *******/
                    '<%= srcPath + grunt.task.current.args[1] %>/js/*.js'
                ],
                tasks: [

                    /*
                    ** JS
                    *******/
                    'js:<%= grunt.task.current.args[1] %>'
                ],
            }
        },

        htmlmin     : {
            dev: {        
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    removeTagWhitespace: true
                },  
                files: [{
                    expand: true,
                    cwd: '<%= destPath %>www/',
                    src: [
                        '*.html'
                    ],
                    dest: '<%= destPath %>www/'
              }]
            }
        }
    };

    grunt.initConfig(gruntConfig);

    /*
    ** CARREGANDO MODULOS
    *******/
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-yui-compressor');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    /*
    ** TASK CSS
    *******/
    grunt.registerTask('css', 'Minificando CSS', function (folder) {

        try {

            grunt.task.run([
                'concat:framework:www',
                'concat:general:www',
                'sass:dist:www',  
            ]);

            if (folder !== 'www' && folder !== undefined) {

                grunt.task.run([
                    'concat:subfolder:' + folder,
                    'sass:subfolder:'   + folder,  
                ]);
            }
        } catch (error) {

            grunt.log.warn('Nothing worked, how it should work');
            grunt.log.warn('Error: ', error.message);
        }
    });

    /*
    ** TASK JS
    *******/
    grunt.registerTask('js', 'Minificando JS', function (folder) {

        try {

            grunt.task.run([
                'jshint:files:www', 
                'uglify:plugin:www',
                'uglify:framework:www',
                'uglify:widget:www',
                'uglify:general:www',
            ]);

            if (folder !== 'www' && folder !== undefined) {

                grunt.task.run([
                    'jshint:subfolder:' + folder,
                    'uglify:subfolder:' + folder,
                ]);
            }
        } catch (error) {

            grunt.log.warn('Nothing worked, how it should work');
            grunt.log.warn('Error: ', error.message);
        }
    });

    /*
    ** HTML TASK
    *******/
    grunt.registerTask('html', 'Minificando HTML', function (folder) {

        try {

            grunt.task.run([
                'includes:dist:www',
                'htmlmin:dev'
            ]);

            if (folder !== 'www' && folder !== undefined) {

                grunt.task.run([
                    'includes:dist:' + folder
                ]);
            }
        } catch (error) {

            grunt.log.warn('Nothing worked, how it should work');
            grunt.log.warn('Error: ', error.message);
        }
    });

    /*
    ** TASK DO MY JOB
    *******/
    grunt.registerTask('domyjob', 'Faça o meu trabalho', function (folder) {

        var srcPath     = gruntConfig.srcPath   + (folder || ''),
            destPath    = gruntConfig.destPath  + ('www/' + (folder || ''));

        try {

            if (grunt.file.exists(srcPath)) {

                if (grunt.file.exists(destPath)) {

                    grunt.task.run([

                        /*
                        ** CSS FOR WWW
                        *******/
                        'concat:framework:www',
                        'concat:general:www',
                        'sass:dist:www',  

                        /*
                        ** JS
                        *******/
                        'jshint:files:www', 
                        'uglify:plugin:www',
                        'uglify:framework:www',
                        'uglify:widget:www',
                        'uglify:general:www'
                    ]);

                    if (folder !== 'www' && folder !== undefined) {

                        grunt.task.run([

                            /*
                            ** CSS FOR ANOTHER FOLDERS
                            *******/
                            'concat:subfolder:' + folder,
                            'sass:subfolder:'   + folder,  

                            /*
                            ** JS
                            *******/
                            'jshint:subfolder:' + folder,
                            'uglify:subfolder:' + folder
                        ]);
                    }

                    grunt.task.run([

                        /*
                        ** HTML
                        ******/
                        'includes:dist:' + (folder || 'www'),
                        'htmlmin:dev',

                        /*
                        ** WATCH
                        *******/
                        'concurrent:project:' + (folder || 'www')
                    ]);
                } else {

                    grunt.log.warn('Destination Folder not Found!');
                }
            } else {

                grunt.log.warn('Source Folder not Found!');
            }
        } catch (error) {

            grunt.log.warn('Nothing worked, how it should work');
            grunt.log.warn('Error: ', error.message);
        }
    });
};